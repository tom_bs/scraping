from datetime import datetime
from flask import Flask, render_template, url_for, request, redirect
import jsonlines
import json
import os
import re
import shutil

# Ugly hack to import sibling module
import sys
sys.path.append("..")
from car_rental import RESULTS_DIRECTORY, METADATA_FILE_NAME

app = Flask(__name__)


@app.route('/')
def index():
  return render_template('index.html')


@app.route('/car_rental')
def car_rental_base():
  return render_template('car_rental.html', title="Car Rental Scraping")


@app.route('/car_rental/view')
def car_rental_view():
  return render_template('car_rental_results.html', title='Car Rental Scraping Results')


@app.route('/api/car_rental/<request_id>/<run_id>')
def car_rental_data(request_id, run_id):
  # TODO(tombs): Replace with DB ?
  with jsonlines.open(os.path.join(RESULTS_DIRECTORY, request_id, run_id + ".jl")) as car_reader:
    cars = []
    for car in car_reader:
      # Transform the car to FE friendly dict
      start_time = datetime.strptime(car['start_time'], '%Y-%m-%d %H:%M')
      end_time = datetime.strptime(car['end_time'], '%Y-%m-%d %H:%M')
      car['summary'] = car.pop('car_string')
      car['price'] = '$' + str(car['price'])
      car['vendor'] = re.sub('Rental Company', '', car['vendor'], flags=re.IGNORECASE).strip()
      if car['start_latlong'] is not None:
        car["start_maps_link"] = f"http://www.google.com/maps/place/{tuple(car['start_latlong'])}"
      if car['end_latlong'] is not None:
        car["end_maps_link"] = f"http://www.google.com/maps/place/{tuple(car['end_latlong'])}"
      if car['review'] is None:
        car['review'] = ""
      if car['persons'] is None:
        car['persons'] = ""
      cars.append(car)

  return {'data': cars}


@app.route('/api/car_rental_requests')
def car_rental_requests_base():
  dirs = [f for f in os.listdir(RESULTS_DIRECTORY) if os.path.isdir(os.path.join(RESULTS_DIRECTORY, f))]
  requests = []
  for request_id in dirs:
    app.logger.info("dir is: " + request_id)
    with open(os.path.join(RESULTS_DIRECTORY, request_id, METADATA_FILE_NAME), 'r') as f:
      request = json.load(f)
      request['id'] = request_id  # needed to save state

      if 'allowed_hours' not in request:
        request['allowed_hours'] = ""
      if 'max_distance' not in request:
        request['max_distance'] = ""
      if 'max_drive_time' not in request:
        request['max_drive_time'] = ""
      if 'min_start_time' not in request:
        request['min_start_time'] = request['start_time']
      if 'max_start_time' not in request:
        request['max_start_time'] = "--"
      if 'min_end_time' not in request:
        request['min_end_time'] = request['end_time']
      if 'max_end_time' not in request:
        request['max_end_time'] = "--"
    run_filenames = [f for f in os.listdir(os.path.join(RESULTS_DIRECTORY, request_id))
                     if os.path.isfile(os.path.join(RESULTS_DIRECTORY, request_id, f)) and f != METADATA_FILE_NAME]
    runs = [{
      'run_time': datetime.fromtimestamp(int(os.path.splitext(rf)[0])).strftime('%Y-%m-%d %H:%M'),
      'request_id': request_id,
      'id': os.path.splitext(rf)[0],
      # Params needed to be passed to ajax
      'view_url': url_for('car_rental_view', request_id=request_id, run_id=int(os.path.splitext(rf)[0])),
      'delete_url': url_for('car_rental_delete_run'),
    } for rf in run_filenames]
    request['runs'] = runs

    requests.append(request)
  return {'data': requests}


@app.route('/car_rental/delete_run', methods=['POST'])
def car_rental_delete_run():
  request_id = request.form['request_id']
  run_id = request.form['run_id']
  filename = os.path.join(RESULTS_DIRECTORY, request_id, run_id + ".jl")
  os.remove(filename)

  run_filenames = [f for f in os.listdir(os.path.join(RESULTS_DIRECTORY, request_id))
                   if os.path.isfile(os.path.join(RESULTS_DIRECTORY, request_id, f)) and f != METADATA_FILE_NAME]
  if len(run_filenames) == 0:
    shutil.rmtree(os.path.join(RESULTS_DIRECTORY, request_id))

  return redirect(request.referrer)
  # # Do nothing
  # return ('', 204)


# TODO(tombs): Process new request
# TODO(tombs): Add schedules
# TODO(tombs): Add progress
@app.route('/car_rental/request', methods=['GET', 'POST'])
def car_rental_request():
    if request.method == 'POST':
        start_location = request.form.get('start_location')
        start_date = request.form.get('start_date')
        return redirect(url_for('car_rental_base'))
        # return '''
        #           <h1>The language value is: {}</h1>
        #           <h1>The framework value is: {}</h1>'''.format(language, framework)

    return render_template('car_rental_request.html', title='Car Rental Request')

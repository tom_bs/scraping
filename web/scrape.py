# Inspired by https://medium.com/analytics-vidhya/integrating-scrapy-with-flask-8611debc4579
# There are problems running scrapy with its twister reactor in a flask HTTP server
# TODO(tombs): Actually probably the simplest idea would be to run two process and talk through files
#  then we don't need Crochet or Celery etc.
# TODO(tombs): Actually could use Celery beat (in antoher process just as a "crontab")
#  https://docs.celeryproject.org/en/stable/userguide/periodic-tasks.html
import crochet
crochet.setup()

from flask import Flask, render_template, jsonify, request, redirect, url_for
from scrapy import signals
from scrapy.crawler import CrawlerRunner
from scrapy.signalmanager import dispatcher
import time
import os
# Importing our Scraping Function

import car_rental as cr


app = Flask(__name__)

output_data = []
crawl_runner = CrawlerRunner()


# By Deafult Flask will come into this when we run the file
@app.route('/hello')
def index():
  return render_template("index.html")  # Returns index.html file in templates folder.


# After clicking the Submit Button FLASK will come into this
@app.route('/', methods=['POST'])
def submit():
  if request.method == 'POST':
    s = request.form['url']  # Getting the Input Amazon Product URL
    global baseURL
    baseURL = s

    # This will remove any existing file with the same name so that the scrapy will not append the data to any previous file.
    if os.path.exists("<path_to_outputfile.json>"):
      os.remove("<path_to_outputfile.json>")

    return redirect(url_for('scrape'))  # Passing to the Scrape function


@app.route("/scrape")
def scrape():
  scrape_with_crochet(baseURL=baseURL)  # Passing that URL to our Scraping Function

  time.sleep(20)  # Pause the function while the scrapy spider is running

  return jsonify(output_data)  # Returns the scraped data after being running for 20 seconds.


@crochet.run_in_reactor
def scrape_with_crochet(baseURL):
  # This will connect to the dispatcher that will kind of loop the code between these two functions.
  dispatcher.connect(_crawler_result, signal=signals.item_scraped)

  # This will connect to the ReviewspiderSpider function in our scrapy file and after each yield will pass to the crawler_result function.
  eventual = crawl_runner.crawl(cr.ExpediaSpider, category=baseURL)
  return eventual


# This will append the data to the output data list.
def _crawler_result(item, response, spider):
  output_data.append(dict(item))


if __name__ == "__main__":
  app.run(debug=True)
To start the flask server:
1. cd to the root flask folder: e.g. `cd C:\Users\TomBS\Documents\scraping\web`
2. Export the name of the python app `set FLASK_APP=hello`
3. [OPTIONAL] `set FLASK_ENV=development`
4. `flask run`
5. [BY DEFAULT FOR DEV] http://127.0.0.1:5000/

Regex tricks:
Not include bad word: ^((?!badword).)*$
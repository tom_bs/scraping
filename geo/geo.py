import geopy
from diskcache import FanoutCache
from enum import Enum
from geopy.geocoders import Nominatim
from geopy import distance
import geopandas
# Note: used https://stackoverflow.com/a/58943939 for installing
import osmnx as ox
import networkx as nx
import os

YEAR_IN_SECS = 60 * 60 * 24 * 365
DATASETS_DIRECTORY = os.path.join(os.path.dirname(__file__), 'datasets')
NATURAL_EARTH_FILE = os.path.join(DATASETS_DIRECTORY, 'ne_10m_populated_places.zip')
EARTH_WORKS_FILE = os.path.join(DATASETS_DIRECTORY, 'stanford-bx729wr3020-shapefile.zip')
CACHE_DIRECTORY = os.path.join(os.path.dirname(__file__), 'cache')
OSMNX_CACHE_DIRECTORY = os.path.join(CACHE_DIRECTORY, 'osmnx')
ox.config(use_cache=True, log_console=True, cache_folder=OSMNX_CACHE_DIRECTORY)
cache = FanoutCache(directory=CACHE_DIRECTORY, shards=8, timeout=2)


class CitiesMethod(Enum):
  # Major cities, world. https://www.naturalearthdata.com/downloads/10m-cultural-vectors/10m-populated-places/
  NATURAL_EARTH_WORLD = 0
  # Exhaustive, just USA. https://earthworks.stanford.edu/catalog/stanford-bx729wr3020
  # Too detailed (and therefore slow)
  EARTH_WORKS_US = 1
  # Like EARTH_WORKS_US but aggregates smaller towns into Nominatim city zoom (e.g. parishes)
  # Still slow because it mostly filters post facto
  EARTH_WORKS_US_AGGREGATED = 2
  # Find OSM point of interest tagged as cities or towns. slow.
  OSMNX = 3


def close_cities(origin, radius=100_000, max_drive_time=None, method=CitiesMethod.NATURAL_EARTH_WORLD):
  """
  Retrieves cities within radius meters from origin according to specific method. To utilize caching, recommended to
  keep using same parameters when possible.
  :param origin: Origin point. Can be free text, (lat, long) or GeoPy location as returned by get_osm_location
  :param max_drive_time: If specified, calculated filter cities whose drive time from origin is greater than this value
   in seconds. It builds a graph of roads, slow. Use None to avoid.
  :return: List of GeoPy locations after filtering. If max_drive_time is used, returns list of tuples
  (location, travel_time_sec).
  """
  radius = round(radius)
  if not isinstance(method, CitiesMethod):
    raise ValueError("method should be instance of the CitiesMethod enum")

  origin = osm_location(origin)
  origin_coor = (origin.latitude, origin.longitude)

  cache_key_all = f"get_cities:{origin_coor}:{radius}:{max_drive_time}:{method.name}"
  if cache_key_all in cache:
    return cache[cache_key_all]

  cache_key_unfiltered = f"get_cities:{origin_coor}:{radius}:{method.name}"
  if cache_key_unfiltered in cache:
    cities = cache[cache_key_unfiltered]
  else:
    # Unbox bbox
    north, south, east, west = get_bbox(origin, radius)

    if method == CitiesMethod.NATURAL_EARTH_WORLD:
      cities_pd = geopandas.read_file(NATURAL_EARTH_FILE, bbox=(west, south, east, north))
      cities = [osm_location((city.geometry.y, city.geometry.x)) for city in cities_pd.itertuples()]
      cache.set(cache_key_unfiltered, cities, expire=YEAR_IN_SECS, tag="get_cities_NATURAL_EARTH_WORLD")
    elif method == CitiesMethod.EARTH_WORKS_US_AGGREGATED:
      cities_pd = geopandas.read_file(EARTH_WORKS_FILE, bbox=(west, south, east, north))
      cities = {}
      for city in cities_pd.itertuples():
        if city.pop_2010 < 2000:
          # skip small towns
          continue
        loc = osm_location((city.geometry.y, city.geometry.x))
        if loc.address not in cities:
          cities[loc.address] = loc
      # AGGREGATED Parishes
      cities = [loc for add, loc in cities.items()
              if not any(c in add.split(",")[0].lower() for c in ["parish", "county"])]
    elif method == CitiesMethod.OSMNX:
      geos = ox.geometries.geometries_from_point((origin.latitude, origin.longitude), dist=radius,
                                                 tags={'place': ["city", "town"]})
      raise NotImplementedError(f"Does not support method {method}")
    else:
      raise NotImplementedError(f"Does not support method {method}")

  if max_drive_time is not None:
    cities = filter_location_by_drive_time(origin, cities, radius, max_drive_time)

  cache.set(cache_key_all, cities, expire=YEAR_IN_SECS, tag=f"get_cities_{method.name}")
  return cities


def filter_location_by_drive_time(origin, locations, radius, max_drive_time,
                                  max_distance_to_ignore_approximate_diff=1000):
  """
  :param origin: Origin point. Can be free text, (lat, long) or GeoPy location as returned by get_osm_location
  :param locations: locations to filter
  :param max_distance_to_ignore_approximate_diff: if a node on a highway is closer than this, don't build an exact map
   of the target location.
  :return: list of tuples (geopy_location, travel_time_sec). Only locations with drive time lower than max_drive_time.
  """
  if max_drive_time is None or len(locations) == 0:
    return locations

  origin = osm_location(origin)
  cf = '["highway"~"motorway|motorway_link|trunk|trunk_link"]'  # only highways!
  G = ox.graph_from_point((origin.latitude, origin.longitude), dist=radius, network_type="drive", custom_filter=cf)

  # Add full roads for origin city
  H = ox.graph_from_place(origin.address, network_type="drive")
  G = nx.compose(G, H)

  origin_node = ox.nearest_nodes(G, origin.longitude, origin.latitude)  # note lat, long are reversed!

  # Add travel time on graph's edges
  G = ox.add_edge_speeds(G)
  G = ox.add_edge_travel_times(G)

  new_locations = []
  for location in locations:
    location = osm_location(location)
    # Skip locations that are not within the radius - they cannot be computed.
    if distance.geodesic((location.latitude, location.longitude), (origin.latitude, origin.longitude)).meters > radius:
      continue

    approximate_target_node = ox.nearest_nodes(G, location.longitude, location.latitude)  # note lat, long are reversed!
    force_exact = False
    try:
      # Get time in seconds to the closest point on the highways
      approximate_travel_time = nx.shortest_path_length(G, origin_node, approximate_target_node, weight="travel_time")
    except nx.NetworkXNoPath:
      # No path on directed graph by highways alone, we need exact
      force_exact = True
    if not force_exact:
      # If we already pass the allowed drive time - skip
      if approximate_travel_time > max_drive_time:
        continue
      # If the approximate target is close - treat it as real target
      approximate_target_point = G.nodes[approximate_target_node]['y'], G.nodes[approximate_target_node]['x']
      if distance.geodesic((location.latitude, location.longitude), approximate_target_point).meters \
        < max_distance_to_ignore_approximate_diff:
        new_locations.append((location, int(approximate_travel_time)))
        continue
    # Resolve using exact location in target place
    H = ox.graph_from_place(location.address, network_type="drive")
    # Add travel time on graph's edges
    H = ox.add_edge_speeds(H)
    H = ox.add_edge_travel_times(H)
    F = nx.compose(G, H)
    target_node = ox.nearest_nodes(F, location.longitude, location.latitude)
    # Get time in seconds
    travel_time = nx.shortest_path_length(F, origin_node, target_node, weight="travel_time")
    if travel_time <= max_drive_time:
      new_locations.append((location, int(travel_time)))

  return new_locations


def get_bbox(origin, dist):
  """Returns (northern lat, southern lat, eastern long, western long) of enclosing bbox distanced dist
  meters from axis"""
  if not isinstance(origin, tuple):
    loc = osm_location(origin)
    origin = (loc.latitude, loc.longitude)

  dist = round(dist)
  north = distance.distance(meters=dist).destination(origin, 0).latitude
  south = distance.distance(meters=dist).destination(origin, 180).latitude
  east = distance.distance(meters=dist).destination(origin, 90).longitude
  west = distance.distance(meters=dist).destination(origin, -90).longitude

  return north, south, east, west


def osm_location_by_point(i, zoom):
  cache_key = f"osm_location:{i}:{zoom}"
  if cache_key in cache:
    return cache[cache_key]

  geolocator = Nominatim(user_agent="TomBS Agent")  # Must change the default user agent
  # Zoom level of smallest city https://nominatim.org/release-docs/latest/api/Reverse/#result-limitation
  location = geolocator.reverse(i, language="en,en-US,en-UK", zoom=zoom)
  cache.set(cache_key, location, expire=YEAR_IN_SECS, tag="osm_location_reverse")
  return location


def osm_location_by_query(i, bbox):
  cache_key = f"osm_location:{i}:{bbox}"
  if cache_key in cache:
    return cache[cache_key]

  geolocator = Nominatim(user_agent="TomBS Agent")  # Must change the default user agent
  nominatim_viewbox = ((90, -180), (-90, 180))
  if bbox:
    north, south, east, west = bbox
    nominatim_viewbox = ((north, west), (south, east))

  location = geolocator.geocode(i, language="en,en-US,en-UK", viewbox=nominatim_viewbox)
  cache.set(cache_key, location, expire=YEAR_IN_SECS, tag="osm_location_geocode")
  return location


def osm_location(i, zoom=13, bbox=None):
  if isinstance(i, geopy.location.Location):
    return i
  elif isinstance(i, tuple):
    return osm_location_by_point(i, zoom)
  else:
    return osm_location_by_query(i, bbox)


def clean_osm_address(location):
  if isinstance(location, geopy.location.Location):
    address = location.address
  elif isinstance(location, str):
    address = location
  else:
    raise ValueError("location should be Nominatim OSM location or address string")

  return ", ".join([p.strip().title() for p in address.split(",")
                    # Clean address from administrative numbers and counties
                    # Some states (e.g. Louisiana) use "Parish" instead of "County"
                    if not any(c in p.lower() for c in ["parish", "county"])
                    and not any(c.isdigit() for c in p)])


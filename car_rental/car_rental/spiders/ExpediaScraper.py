import geopy.location
import scrapy
from datetime import timedelta
import itertools
import json
import geo
from urllib.parse import urlencode, urljoin
# This library seems most compliant with https://tools.ietf.org/id/draft-goessner-dispatch-jsonpath-00.html
import jsonpath_rw_ext as jpath
from utils import get_random_user_agent

EXPEDIA_BASE_URL = "https://www.expedia.com"


class ExpediaCarsRequest:
  def __init__(self, start_location, end_location, start_date, end_date):
    self.start_location = geo.clean_osm_address(start_location)
    self.end_location = geo.clean_osm_address(end_location)
    self.start_date = start_date
    self.end_date = end_date
    self._EXPEDIA_BASE_CARRESULT_URL = urljoin(EXPEDIA_BASE_URL, "graphql")
    self._EXPEDIA_BASE_CARSEARCH_URL = urljoin(EXPEDIA_BASE_URL, "carsearch")

  def generate_scrapy_request(self, *args, **kwargs):
    request = scrapy.Request(
      self._EXPEDIA_BASE_CARRESULT_URL,
      method='POST',
      headers=self.__get_expedia_headers(),
      body=json.dumps(self.__get_expedia_post_params()),
      *args,
      **kwargs)
    # https://doc.scrapy.org/en/latest/topics/request-response.html#passing-additional-data-to-callback-functions
    request.cb_kwargs['expedia'] = self

    return request

  @staticmethod
  def __convert_city_name(city):
    if isinstance(city, geopy.location.Location):
      return ", ".join([p.strip().title() for p in city.address.split(",")
                        # Clean address from administrative numbers and counties
                        # Some states (e.g. Louisiana) use "Parish" instead of "County"
                        if not any(c in p.lower() for c in ["parish", "county"]) and not any(c.isdigit() for c in p)])

    # No extra space, Capital letter on each word (e.g. New York)
    return ' '.join(city.split()).title()

  def __get_expedia_carsearch_params(self):
    return {
      "locn": self.__convert_city_name(self.start_location),
      "loc2": self.__convert_city_name(self.end_location),
      "date1": self.start_date.strftime("%m/%d/%Y"),
      "date2": self.end_date.strftime("%m/%d/%Y"),
      "d1": self.start_date.strftime("%d-%m-%Y"),
      "d2": self.end_date.strftime("%d-%m-%Y"),
      "aarpcr": "off",  # No AARP membership
      "vend": "",  # any vendor
      "time1": self.start_date.strftime("%H%M%p"),
      "time2": self.end_date.strftime("%H%M%p"),
      # From here I don't know what these params mean
      "olat": "",
      "olon": "",
      "dlat": "",
      "dlon": "",
      "dagv": "1",
      "subm": "1",
      "fdrp": "1",
      "ttyp": "2",
      "acop": "2",
      "rdus": "10",
      "rdct": "1",
      "styp": "4"
    }

  def __get_expedia_carsearch_url(self):
    return self._EXPEDIA_BASE_CARSEARCH_URL + "?" + urlencode(self.__get_expedia_carsearch_params())

  def __get_expedia_post_params(self):
    return {
      "operationName": "CarSearchV3",
      "query": "\n        query CarSearchV3(\n    $context: ContextInput!\n    $primaryCarSearchCriteria: PrimaryCarCriteriaInput!\n    $secondaryCriteria: ShoppingSearchCriteriaInput!\n    $shoppingContext: ShoppingContextInput\n) {\n    carSearchOrRecommendations(\n        context: $context\n        primaryCarSearchCriteria: $primaryCarSearchCriteria\n        secondaryCriteria: $secondaryCriteria\n        shoppingContext: $shoppingContext\n    ) {\n        carSearchResults {\n            shoppingContext {\n                multiItem {\n                    id\n                }\n            }\n            carsShoppingContext {\n                searchId\n            }\n            multiItemAnchorPriceToken\n            multiItemPickUpDropOffMessage {\n                message {\n                    __typename\n                    ... on CarPhraseText {\n                        text\n                    }\n                }\n            }\n            multiItemPlayBackTitleBar {\n                primary {\n                    __typename\n                    ... on CarPhraseText {\n                        text\n                    }\n                }\n                secondary {\n                    longMessage {\n                        __typename\n                        ... on CarPhraseText {\n                            text\n                        }\n                    }\n                    shortMessage {\n                        __typename\n                        ... on CarPhraseText {\n                            text\n                        }\n                    }\n                    accessibilityMessage {\n                        __typename\n                        ... on CarPhraseText {\n                            text\n                        }\n                    }\n                }\n            }\n            listings {\n                __typename\n                ... on CarMessagingCard {\n                    ...messagingCard\n                }\n                ... on CarSaleCard {\n                    ...saleCard\n                }\n                ... on CarOfferCard {\n                    accessibilityString\n                    offerBadges {\n                        ...badge\n                    }\n                    vehicle {\n                        image {\n                            ...image\n                        }\n                        category\n                        description\n                        attributes {\n                            icon {\n                                ...icon\n                            }\n                            text\n                        }\n                        features {\n                            ...feature\n                        }\n                        fuelInfo {\n                            ...feature\n                        }\n                    }\n                    tripLocations {\n                        pickUpLocation {\n                            ...carOfferVendorLocationInfo\n                        }\n                        dropOffLocation {\n                            ...carOfferVendorLocationInfo\n                        }\n                    }\n                    vendor {\n                        image {\n                            ...image\n                        }\n                    }\n                    detailsContext {\n                        carOfferToken\n                        selectedAccessories\n                        rewardPointsSelection\n                        continuationContextualId\n                    }\n                    priceBadges {\n                        ...badge\n                    }\n                    actionableConfidenceMessages {\n                        ...carPhrase\n                    }\n                    review {\n                        rating\n                        superlative\n                    }\n                    priceSummary {\n                        ...offerPriceSummary\n                    }\n                    shortList {\n                        favorited\n                        saveUrl\n                        removeUrl\n                    }\n                    action {\n                        ...carAction\n                    }\n                    reserveButtonText\n                    infositeURL {\n                        relativePath\n                    }\n                    offerHeading\n                    multiItemPriceToken\n                    additionalBenefits {\n                        ...carPhrase\n                    }\n                    cancellationAndPaymentMessages {\n                        ...carPhrase\n                    }\n                    clubbedPriceOptions {\n                        dialogTrigger {\n                            ...carActionableItem\n                        }\n                        dialogContent {\n                            title\n                            vendor {\n                                image {\n                                    ...image\n                                }\n                            }\n                            vehicleCategory\n                            vehicleDescription\n                            optionsSummary {\n                                ...carPhrase\n                            }\n                            clubbedPrices {\n                                offerBadges {\n                                    ...badge\n                                }\n                                priceBadges {\n                                    ...badge\n                                }\n                                priceSummary {\n                                    ...offerPriceSummary\n                                }\n                                paymentMessages {\n                                    ...carPhrase\n                                }\n                                cancellationMessages {\n                                    ...carPhrase\n                                }\n                                additionalBenefits {\n                                    ...carPhrase\n                                }\n                                reserveButtonAction {\n                                    ...carAction\n                                }\n                                reserveButtonText\n                                infositeURL {\n                                    relativePath\n                                }\n                            }\n                            commonAdditionalBenefits {\n                                ...carPhrase\n                            }\n                            modalFooter\n                        }\n                    }\n                    unlockOffer {\n                        ...carActionableItem\n                    }\n                    isFareComparisonTestEnabled\n                    priceSummaryText\n\n                    enhancedCleanlinessDialog {\n                        images {\n                            value\n                        }\n                        title\n                        description\n                        content {\n                            title {\n                                icon {\n                                    ...icon\n                                }\n                                text\n                            }\n                            infos\n                        }\n                        carouselButtonAllyStrings\n                    }\n                }\n            }\n            carRentalLocations {\n                routeType\n                pickUpLocation {\n                    ...carSearchLocation\n                }\n                dropOffLocation {\n                    ...carSearchLocation\n                }\n            }\n            changeRentalLocation {\n                link {\n                    ... on CarActionableItem {\n                        text\n                    }\n                }\n                placeHolder {\n                    ... on CarPhraseText {\n                        text\n                    }\n                }\n                primary {\n                    ... on CarPhraseText {\n                        text\n                    }\n                }\n                secondary {\n                    ... on CarPhraseText {\n                        text\n                    }\n                }\n            }\n            summary {\n                title\n                dynamicTitle {\n                    ...carPhrase\n                }\n                pageTitle\n                includedFeeFooter {\n                    title\n                    description\n                }\n            }\n            filterNoMatchMessage {\n                messagingCard {\n                    ...messagingCard\n                }\n                allOffersTextSeparator\n            }\n            loadMoreAction {\n                action {\n                    ...carAction\n                }\n                icon {\n                    ...icon\n                }\n                searchPagination {\n                    size\n                    startingIndex\n                }\n                text\n            }\n            loyaltyInfo {\n                __typename\n                ... on CarPhraseText {\n                    text\n                }\n                ... on CarActionableItem {\n                    ...carActionableItem\n                }\n                ... on CarPhraseMark {\n                    name\n                    description\n                }\n            }\n            sortAndFilter {\n                title\n                close {\n                    ...icon\n                }\n                appliedFilterCount\n                clearButtonTitle\n                sections {\n                    title\n                    fields {\n                        __typename\n                        primary\n                        secondary\n                        ...SingleSelectionFragment\n                        ...MultiSelectionFragment\n                    }\n                }\n            }\n            carAppliedSortAndFilters {\n                appliedSortAndFilters {\n                    id\n                    text\n                    value\n                    crossIcon {\n                        ...icon\n                    }\n                    analytics {\n                        ...clientSideAnalytics\n                    }\n                }\n            }\n\n            shareFeedbackAction {\n                text\n                button {\n                    ...carActionableItem\n                }\n            }\n\n            adsTargetingData {\n                uuid\n                siteId\n                pageName\n                origin\n                dest\n                locResolver\n                dateStart\n                dateEnd\n                adProvider\n            }\n\n            recommendedSortDisclaimer {\n                ...carPhrase\n            }\n\n          recommendedSortExplanation {\n              content {\n                  ...carPhrase\n              }\n              feedback {\n                  ...searchUserFeedback\n              }\n              closeAnalytics {\n                  ...analytics\n              }\n          }\n\n            makeModelDisclaimer {\n                ...carPhrase\n            }\n\n            map {\n                title\n                text\n                closeIcon {\n                    ...icon\n                }\n                closeAction {\n                    ...carAction\n                }\n                center {\n                    ...coordinates\n                }\n                bounds {\n                    northeast {\n                        ...coordinates\n                    }\n                    southwest {\n                        ...coordinates\n                    }\n                }\n                zoomLevel\n                markers {\n                    __typename\n                    type\n                    action {\n                        ...carAction\n                    }\n                    coordinates {\n                        ...coordinates\n                    }\n                    ... on CarItemCardMapMarker {\n                        itemCard {\n                            ... on CarMapSearchLocationCard {\n                                title\n                                text\n                            }\n                            ... on CarMapPickupLocationCard {\n                                vendorLocationId\n                                vendorImage {\n                                    ...image\n                                }\n                                address\n                                distance\n                                superlative\n                                priceSummary {\n                                    lead {\n                                        ...priceInfo\n                                    }\n                                    total {\n                                        ...priceInfo\n                                    }\n                                    strikeThroughFirst\n                                    loyaltyPointsOption {\n                                        formattedPoints\n                                        leadingCaption\n                                        pointsFirst\n                                    }\n                                    accessibility\n                                }\n                                seeCarsButtonText\n                                action {\n                                    ...carAction\n                                }\n                            }\n                        }\n                    }\n                }\n                mapButton {\n                    url {\n                        value\n                    }\n                    text\n                    action {\n                        ...carAction\n                    }\n                }\n            }\n        }\n        carsErrorContent {\n            icon {\n                ...icon\n            }\n            heading\n            subText\n            analytics {\n                linkName\n                referrerId\n            }\n        }\n        carRecommendationContent {\n            ... on CarRecommendations {\n                ...carRecommendations\n            }\n        }\n    }\n}\n\nfragment saleCard on CarSaleCard {\n    cardDescription {\n        ...carPhrase\n    }\n    cardTitle {\n        ...carPhrase\n    }\n    linkText {\n        ...carPhrase\n    }\n    cardAction {\n        actionType\n        analytics {\n            linkName\n            referrerId\n        }\n    }\n    image {\n        ...image\n    }\n    analytics {\n        linkName\n        referrerId\n    }\n}\n\nfragment messagingCard on CarMessagingCard {\n    title\n    description\n    descriptions\n    mark\n    illustrationURL {\n        value\n    }\n    icon {\n        ...icon\n    }\n    links {\n        text\n        url {\n            relativePath\n        }\n        icon {\n            ...icon\n        }\n        action {\n            actionType\n            analytics {\n                linkName\n                referrerId\n            }\n        }\n    }\n    dialog {\n        title\n        buttonText\n        text\n        content {\n            header {\n                ...carPhrase\n            }\n            body {\n                title {\n                    ...carPhrase\n                }\n                body {\n                    ...carPhrase\n                }\n            }\n            footer {\n                ...carPhrase\n            }\n        }\n        type\n    }\n    analytics {\n        ...analytics\n    }\n}\n\nfragment badge on CarOfferBadge {\n    icon {\n        ...icon\n    }\n    text\n    theme\n    mark {\n        id\n    }\n}\n\nfragment feature on VehicleFeature {\n    icon {\n        ...icon\n    }\n    text\n    info {\n        vehicleFeatureDialog {\n            title\n            text\n            buttonText\n        }\n        carActionableItem {\n            ...carActionableItem\n        }\n    }\n}\nfragment priceInfo on CarPriceInfo {\n    price {\n        ...money\n    }\n    accessibility\n    qualifier\n    formattedValue\n}\nfragment carActionableItem on CarActionableItem {\n    url {\n        value\n        relativePath\n    }\n    action {\n        ...carAction\n    }\n    text\n    icon {\n        ...icon\n    }\n}\nfragment dialogContentCarPhrase on CarPhrase {\n    __typename\n    ... on CarPhraseText {\n        text\n    }\n    ... on CarPhraseIconText {\n        text\n        icon {\n            ...icon\n        }\n    }\n    ... on CarsRichText {\n        value\n        style\n    }\n    ... on CarActionableItem {\n        ...carActionableItem\n    }\n}\nfragment carDialogConfidenceMessage on CarDialogConfidenceMessage {\n    text\n    icon {\n        ...icon\n    }\n    openDialogAction {\n        ...carAction\n    }\n    dialogContent {\n        title\n        text\n        buttonText\n        content {\n            header {\n                ...dialogContentCarPhrase\n            }\n            body {\n                __typename\n                title {\n                    ...dialogContentCarPhrase\n                }\n                body {\n                    ...dialogContentCarPhrase\n                }\n            }\n            footer {\n                ...dialogContentCarPhrase\n            }\n        }\n        type\n    }\n    iconMobileRender\n}\nfragment carAction on CarAction {\n    actionType\n    accessibility\n    analytics {\n        ...analytics\n    }\n}\nfragment icon on Icon {\n    description\n    id\n    withBackground\n    size\n}\nfragment image on Image {\n    description\n    url\n}\nfragment money on Money {\n    amount\n    formatted\n    currencyInfo {\n        code\n        name\n        symbol\n    }\n}\nfragment analytics on CarAnalytics {\n    linkName\n    referrerId\n}\nfragment clientSideAnalytics on ClientSideAnalytics {\n    linkName\n    referrerId\n}\nfragment Expando on ShoppingSelectionExpando {\n    expandLabel\n    collapseLabel\n    visible\n    threshold\n    minimalHeight\n    analytics {\n        ...clientSideAnalytics\n    }\n}\nfragment MultiSelectionFragment on ShoppingMultiSelectionField {\n    primary\n    secondary\n    options {\n        ...FilterOption\n    }\n    expando {\n        ...Expando\n    }\n}\nfragment SingleSelectionFragment on ShoppingSelectionField {\n    primary\n    secondary\n    options {\n        ...FilterOption\n    }\n    expando {\n        ...Expando\n    }\n}\nfragment FilterOption on ShoppingSelectableFilterOption {\n    id\n    value\n    primary\n    secondary\n    description\n    accessibility\n    selected\n    disabled\n    icon {\n        ...icon\n    }\n    analytics {\n        ...clientSideAnalytics\n    }\n}\nfragment coordinates on Coordinates {\n    latitude\n    longitude\n}\n\nfragment carOfferVendorLocationInfo on CarOfferVendorLocationInfo {\n    icon {\n        ...icon\n    }\n    text\n    locationSubInfo\n}\nfragment detailsRichText on CarsRichText {\n    value\n    theme\n    style\n}\n\nfragment carRecommendations on CarRecommendations {\n    __typename\n    heading\n    analytics {\n        linkName\n        referrerId\n    }\n    carRecommendationsCards {\n        ...carRecommendationCard\n    }\n}\n\nfragment carRecommendationCard on CarRecommendationCard {\n    __typename\n    icon {\n        ...icon\n    }\n    action {\n        ...carAction\n    }\n\n    ... on DateTimeRecommendationCard {\n        icon {\n            ...icon\n        }\n        text {\n            ...detailsRichText\n        }\n        subText {\n            ...carPhrase\n        }\n        action {\n            ...carAction\n        }\n    }\n\n    ... on LocationRecommendationCard {\n        icon {\n            ...icon\n        }\n        location {\n            ...detailsRichText\n        }\n        city {\n            ...carPhrase\n        }\n        distanceText {\n            ...carPhrase\n        }\n        action {\n            ...carAction\n        }\n        pickUpLocation {\n            ...carSearchLocation\n        }\n    }\n\n    ... on PartnerRecommendationCard {\n        icon {\n            ...icon\n        }\n        action {\n            ...carAction\n        }\n        analytics {\n            ...analytics\n        }\n        image {\n            ...image\n        }\n        title {\n            ...carPhrase\n        }\n        subText {\n            ...carPhrase\n        }\n        button {\n            ...carActionableItem\n        }\n        dialog {\n            ... on PartnerRecommendationDialog {\n                title {\n                    ...carPhrase\n                }\n                subTitle {\n                    ...carPhrase\n                }\n                text {\n                    ...carPhrase\n                }\n                button {\n                    ...carActionableItem\n                }\n                closeDialog {\n                    ...carAction\n                }\n                content {\n                    ... on PartnerRecommendationDialogContent {\n                        title {\n                            ...carPhrase\n                        }\n                        body {\n                            ...carPhrase\n                        }\n                        confidenceMessage {\n                            ...carPhrase\n                        }\n                    }\n                }\n                image {\n                    ...image\n                }\n            }\n        }\n    }\n}\nfragment carPhrase on CarPhrase {\n    __typename\n    ... on CarPhraseText {\n        text\n    }\n    ... on CarPhrasePairText {\n        richText {\n            ...detailsRichText\n        }\n        richSubText {\n            ...detailsRichText\n        }\n    }\n    ... on CarPhraseIconText {\n        text\n        icon {\n            ...icon\n        }\n    }\n    ... on CarActionableItem {\n        ...carActionableItem\n    }\n    ... on CarDialogConfidenceMessage {\n        ...carDialogConfidenceMessage\n    }\n    ... on CarsRichText {\n        value\n        style\n        theme\n    }\n    ... on CarPhraseMark {\n        description\n        name\n    }\n    ... on CarOfferBadge {\n        icon {\n            ...icon\n        }\n        mark {\n            id\n        }\n        text\n        badgeTheme: theme\n    }\n}\nfragment carSearchLocation on CarSearchLocation {\n    shortName\n    fullName\n    regionId\n}\n\nfragment offerPriceSummary on CarPriceSummary {\n    discountBadge {\n        ...badge\n    }\n    lead {\n        ...priceInfo\n    }\n    total {\n        ...priceInfo\n    }\n    strikeThroughFirst\n    loyaltyPointsOption {\n        formattedPoints\n        leadingCaption\n        pointsFirst\n    }\n    accessibility\n    reference {\n        additionalInfo\n        price {\n            ...money\n        }\n    }\n    paymentInfo {\n        icon {\n            ...icon\n        }\n        text\n        additionalPaymentInfo\n    }\n    priceAdditionalInfo\n}\n\nfragment searchUserFeedback on UserFeedback {\n    userSurveyTitle\n    options {\n        inputHeading\n        inputTextPlaceholder\n        option {\n            ...carActionableItem\n        }\n    }\n    submitConfirmation\n    submit {\n        ...carActionableItem\n    }\n}\n\n    ",
      "variables": {
        "context": {
          "siteId": 1,
          "locale": "en_US",
          "eapid": 0,
          "currency": "USD",
          "device": {
            "type": "DESKTOP"
          },
          "identity": {
            "duaid": "1eb52c8b-540d-45ec-9798-a476a7f6e33e",
            "expUserId": "-1",
            "tuid": "-1",
            "authState": "ANONYMOUS"
          },
          "debugContext": {
            "abacusOverrides": [],
            "alterMode": "RELEASED"
          }
        },
        "primaryCarSearchCriteria": {
          "pickUpLocation": {
            "searchTerm": f"{self.start_location}"
          },
          "dropOffLocation": {
            "searchTerm": f"{self.end_location}"
          },
          "pickUpDateTime": {
            "day": self.start_date.day,
            "month": self.start_date.month,
            "year": self.start_date.year,
            "hour": self.start_date.hour,
            "minute": self.start_date.minute,
            "second": 0
          },
          "dropOffDateTime": {
            "day": self.end_date.day,
            "month": self.end_date.month,
            "year": self.end_date.year,
            "hour": self.end_date.hour,
            "minute": self.end_date.minute,
            "second": 0
          }
        },
        "secondaryCriteria": {
          "booleans": [
            {
              "id": "SALES_UNLOCKED",
              "value": False
            }
          ],
          "selections": [
            {
              "id": "rdus",
              "value": "10"
            },
            {
              "id": "selPageIndex",
              "value": "0"
            },
            {
              "id": "selSort",
              # Could also be ["RECOMMENDED", "TOTAL_PRICE_LOW_TO_HIGH"]
              "value": "TOTAL_PRICE_LOW_TO_HIGH"
            },
            {
              "id": "searchId",
              "value": ""
            }
          ]
        },
        "shoppingContext": None
      },
    }

  def __get_expedia_headers(self):
    return {
      # Most popular user agent. Also needed to get desktop result.
      'User-Agent': get_random_user_agent(),
      'x-page-id': 'page.Car-Search,C,20',
      'pragma': 'no-cache',
      'content-type': 'application/json',
      # Handled by base middleware DefaultHeadersMiddleware
      # 'content-length': '',
      'client-info': 'bernie-cars-shopping-web,pwa,external, domain-redirect:false',
      'origin': 'https://www.expedia.com',
      'referer': self.__get_expedia_carsearch_url(),
      'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="98", "Google Chrome";v="98"',
      'sec-ch-ua-mobile': '?0',
      'sec-ch-ua-platform': '"Windows"',
      'sec-fetch-dest': 'empty',
      'sec-fetch-mode': 'cors',
      'sec-fetch-site': 'same-origin',
      'sec-fetch-user': '?1',
      'upgrade-insecure-requests': '1',
      'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
      'accept-encoding': 'gzip, deflate, br',
      # Remove hebrew
      'accept-language': 'en-US;q=0.9,en-GB;q=0.8',
      'cache-control': 'no-cache'
    }


class ExpediaSpider(scrapy.Spider):
  """
  TODO(tombs): Add routes. Add locations and (min, max) duration for when to get there from last point.
   Naive algorithm: (without thinking of close cities) get the cross product of all possible routes. Run
   an independent instance of the current scrapy between all cities (and their surrounding). Compute the best options.
  TODO(tombs): Add page to submit a new request that runs scrapy.
  TODO(tombs): Add schedules.
  """
  name = "expedia"
  download_delay = 0  # Do not delay

  def __init__(self, start_time, start_location, end_time, end_location, max_start_time=None, max_end_time=None,
               allowed_hours=None, max_distance=None, max_drive_time=None):
    super().__init__(ExpediaSpider.name)
    self._locations_cache = {}
    self.start_location = geo.osm_location(start_location)
    self.end_location = geo.osm_location(end_location)
    self.min_start_time = start_time
    self.max_start_time = max_start_time if max_start_time is not None else start_time
    self.min_end_time = end_time
    self.max_end_time = max_end_time if max_end_time is not None else end_time
    self.allowed_hours = allowed_hours
    self.max_distance = max_distance
    self.max_drive_time = max_drive_time
    self.start_bbox, self.end_bbox = None, None
    if max_distance:
      self.start_bbox = geo.get_bbox(self.start_location, max_distance)
      self.end_bbox = geo.get_bbox(self.end_location, max_distance)

  def _get_locations(self):
    locations = []
    if self.max_distance is None:
      locations.append((self.start_location, self.end_location))
    else:
      # TODO(tombs): Expedia seems to include locations in radius up to 25 miles (~40km), could filter more cities
      # TODO(tombs): Filter more cities by setting scrapy priority and see there are results
      start_cities = geo.close_cities(self.start_location, self.max_distance, self.max_drive_time)
      end_cities = geo.close_cities(self.end_location, self.max_distance, self.max_drive_time)
      for start_city, end_city in itertools.product(start_cities, end_cities):
        locations.append((start_city, end_city))
    return locations

  def _get_times(self):
    times = []
    start = self.min_start_time
    while start <= self.max_start_time:
      end = self.min_end_time
      while end <= self.max_end_time:
        if self.allowed_hours is None:
          times.append((start, end))
        else:
          for start_hour in self.allowed_hours:
            for end_hour in self.allowed_hours:
              times.append((
                start.replace(hour=start_hour.hour, minute=start_hour.minute),
                end.replace(hour=end_hour.hour, minute=end_hour.minute)))
        end += timedelta(days=1)
      start += timedelta(days=1)
    return times

  def start_requests(self):
    for (start_location, end_location), (start_time, end_time) in itertools.product(self._get_locations(),
                                                                                    self._get_times()):
      route = ExpediaCarsRequest(start_location, end_location, start_time, end_time)
      yield route.generate_scrapy_request(callback=self.parse)
    # TODO(tombs): Add progress, see signals https://docs.scrapy.org/en/latest/topics/signals.html

  def parse(self, response, expedia):
    json_response = response.json()
    for car_offer in jpath.match('$..listings[?(@.__typename="CarOfferCard")]', json_response):
      s_jloc = jpath.match1('$..pickUpLocation.locationSubInfo', car_offer)
      start_partial_loc = f"{s_jloc[2]}, {s_jloc[0]}"
      start_loc = geo.osm_location(start_partial_loc, bbox=self.start_bbox)
      start_address = geo.clean_osm_address(start_loc) if start_loc is not None else start_partial_loc
      # City zoom
      # TODO(tombs): concise sometimes lose the city, using normal address for now
      start_address_concise = geo.clean_osm_address(geo.osm_location((start_loc.latitude, start_loc.longitude)))\
        if start_loc is not None else start_partial_loc
      start_lat_long = (start_loc.latitude, start_loc.longitude) if start_loc is not None else None
      d_jloc = jpath.match1('$..dropOffLocation.locationSubInfo', car_offer)
      if d_jloc is None:
        # Same as pickup
        d_jloc = s_jloc
      end_partial_loc = f"{d_jloc[2]}, {d_jloc[0]}"
      end_loc = geo.osm_location(end_partial_loc, bbox=self.end_bbox)
      end_address = geo.clean_osm_address(end_loc) if end_loc is not None else end_partial_loc
      # City zoom
      end_address_concise = geo.clean_osm_address(geo.osm_location((end_loc.latitude, end_loc.longitude)))\
        if end_loc is not None else end_partial_loc
      end_lat_long = (end_loc.latitude, end_loc.longitude) if end_loc is not None else None

      reserve_url_relative_path = jpath.match1('$..infositeURL.relativePath', car_offer)
      reserve_url_path = urljoin(EXPEDIA_BASE_URL,
                                 reserve_url_relative_path) if reserve_url_relative_path is not None else None

      yield {
        "car_string": jpath.match1('$.accessibilityString', car_offer),
        "start_time": expedia.start_date.strftime('%Y-%m-%d %H:%M'),
        "end_time": expedia.end_date.strftime('%Y-%m-%d %H:%M'),
        "start_address": start_address,
        "start_address_concise": start_address_concise,
        "start_latlong": start_lat_long,
        "end_address": end_address,
        "end_address_concise": end_address_concise,
        "end_latlong": end_lat_long,
        "token": jpath.match1('$.detailsContext.carOfferToken', car_offer),
        "car_category": jpath.match1('$.vehicle.category', car_offer),
        "car_description": jpath.match1('$.vehicle.description', car_offer),
        "image_url": jpath.match1('$.vehicle.image.url', car_offer),
        "persons": jpath.match1('$.vehicle.attributes[?(@..description="person")].text', car_offer),
        "doors": jpath.match1('$.vehicle.attributes[?(@..description="doors")].text', car_offer),
        "transmission": jpath.match1('$.vehicle.attributes[?(@..description="transmission")].text', car_offer),
        "mileage": jpath.match1('$.vehicle.attributes[?(@..description="mileage")].text', car_offer),
        "vendor": jpath.match1('$.vendor..description', car_offer),
        "price": jpath.match1('$.priceSummary.total..amount', car_offer),
        "review": jpath.match1('$..review.rating', car_offer),
        "reserve_url": reserve_url_path,
        "attributes": jpath.match('$..actionableConfidenceMessages[*].value', car_offer),
      }

  def __find_items(self, o, key, value=None):
    if isinstance(o, list):
      for v in o:
        yield from self.__find_items(v, key, value)
    elif isinstance(o, dict):
      if key in o:
        if value is None or o[key] == value:
          yield o
      else:
        for k, v in o.items():
          yield from self.__find_items(v, key, value)

import os

RESULTS_DIRECTORY = os.path.join(os.path.dirname(__file__), 'results')
METADATA_FILE_NAME = "params.json"

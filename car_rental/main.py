from datetime import datetime, time
import geo
import hashlib
import json
from scrapy.crawler import CrawlerProcess, CrawlerRunner
from scrapy.utils.log import configure_logging
from scrapy.utils.project import get_project_settings
from car_rental.spiders.ExpediaScraper import ExpediaSpider
import os
from time import time as timeit, ctime
from settings import RESULTS_DIRECTORY, METADATA_FILE_NAME
from twisted.internet import defer, reactor
import threading


processes = []


def hash_params(params):
  m = hashlib.md5()

  for k, v in params.items():
    if isinstance(v, list):
      s = str(k)
      for vv in v:
        s += str(v)
    else:
      s = str(k) + str(v)
    m.update(s.encode('utf-8'))
  return m.hexdigest()


def jsonify_params(params):
  new_params = {}
  for k, v in params.items():
    if k in ["start_time", "max_start_time", "end_time", "max_end_time"]:
      new_params[k] = v.strftime('%Y-%m-%d')
    elif k == "allowed_hours":
      new_params[k] = [vv.strftime('%H:%M') for vv in v]
    else:
      new_params[k] = v
  return new_params


def create_process(expedia_params):
  settings = get_project_settings()
  settings['FEED_FORMAT'] = 'jsonlines'
  scrape_directory = os.path.join(RESULTS_DIRECTORY, hash_params(expedia_params))
  file_name = str(int(timeit())) + ".jl"
  scrape_file = os.path.join(scrape_directory, file_name)
  params_path = os.path.join(scrape_directory, METADATA_FILE_NAME)

  settings['FEED_URI'] = "file:///" + scrape_file

  os.makedirs(os.path.dirname(params_path), exist_ok=True)
  with open(params_path, 'w+') as fp:
    json.dump(jsonify_params(expedia_params), fp)

  processes.append((CrawlerRunner(settings), expedia_params))
  # process = CrawlerProcess(settings)
  # process.crawl(ExpediaSpider, **expedia_params)
  # process.start()


@defer.inlineCallbacks
def crawl():
  for process, params in processes:
    yield process.crawl(ExpediaSpider, **params)
  reactor.stop()


def run():
  ##   (1)
  expedia_params = {
    "start_time": datetime(2022, 10, 26),
    "start_location": geo.clean_osm_address(geo.osm_location("Knoxville, Tennessee")),
    "end_time": datetime(2022, 11, 4),
    "max_end_time": datetime(2022, 11, 7),
    "end_location": geo.clean_osm_address(geo.osm_location("New Orleans")),
    "max_distance": 100_000,
    "allowed_hours": [time(10, 30), time(18, 30)],
  }
  create_process(expedia_params)



  ##   (1 - b)
  expedia_params = {
    "start_time": datetime(2022, 10, 26),
    "start_location": geo.clean_osm_address(geo.osm_location("Asheville, North Carolina")),
    "end_time": datetime(2022, 11, 4),
    "max_end_time": datetime(2022, 11, 7),
    "end_location": geo.clean_osm_address(geo.osm_location("New Orleans")),
    "max_distance": 100_000,
    "allowed_hours": [time(10, 30), time(18, 30)],
  }
  create_process(expedia_params)




  ##   (2)
  expedia_params = {
    "start_time": datetime(2022, 11, 7),
    "max_start_time": datetime(2022, 11, 10),
    "start_location": geo.clean_osm_address(geo.osm_location("Charleston, South Carolina")),
    "end_time": datetime(2022, 11, 12),
    "end_location": geo.clean_osm_address(geo.osm_location("Orlando, Florida")),
    "max_distance": 100_000,
    "allowed_hours": [time(10, 30), time(18, 30)],
  }
  create_process(expedia_params)



  ##   (3)
  expedia_params = {
    "start_time": datetime(2022, 11, 15),
    "max_start_time": datetime(2022, 11, 16),
    "start_location": geo.clean_osm_address(geo.osm_location("Orlando, Florida")),
    "end_time": datetime(2022, 11, 20),
    "end_location": geo.clean_osm_address(geo.osm_location("Miami, Florida")),
    "max_distance": 100_000,
    "allowed_hours": [time(10, 30), time(18, 30)],
  }
  create_process(expedia_params)


  ##   (4) - instead of (2+3)
  expedia_params = {
    "start_time": datetime(2022, 11, 7),
    "max_start_time": datetime(2022, 11, 10),
    "start_location": geo.clean_osm_address(geo.osm_location("Charleston, South Carolina")),
    "end_time": datetime(2022, 11, 20),
    "end_location": geo.clean_osm_address(geo.osm_location("Miami, Florida")),
    "max_distance": 100_000,
    "allowed_hours": [time(10, 30), time(18, 30)],
  }
  create_process(expedia_params)


  ##   (5)
  expedia_params = {
    "start_time": datetime(2022, 11, 26),
    "start_location": geo.clean_osm_address(geo.osm_location("Miami, Florida")),
    "end_time": datetime(2022, 11, 30),
    "end_location": geo.clean_osm_address(geo.osm_location("Miami, Florida")),
    "max_distance": 100_000,
    "allowed_hours": [time(10, 30), time(18, 30)],
  }
  create_process(expedia_params)



  crawl()
  reactor.run()


if __name__ == "__main__":
  configure_logging({'LOG_FORMAT': '%(levelname)s: %(message)s'})

  def callback():
    print(f"New run at time: {ctime()}\n\n")
    run()
    HOUR = 60 * 60
    threading.Timer(24 * HOUR, callback).start()

  callback()
